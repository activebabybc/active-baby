Active Baby offers unique and popular baby items like strollers, baby clothes, car seats, and baby toys. Active Baby is a childrens store in Canada and baby stores in North Vancouver, Kitsilano and Langley, BC. They are also a popular online baby store Canada! Shop and buy online now!

Address: 1985 Lonsdale Ave, North Vancouver, BC V7M 2K3, Canada

Phone: 604-986-8977

Website: https://www.activebaby.ca
